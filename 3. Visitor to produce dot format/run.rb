require_relative 'json_lexer'
require_relative 'JsonParser'
require_relative 'DotVisitor'

# Change to other example as needed
file_path = File.expand_path('../examples/example0.json', __dir__)

lexer = JsonLexer.new(File.open(file_path, 'r'))

parser = JsonParser.new(lexer)

parsed_json = parser.parse

puts parsed_json.inspect

# Create a DotVisitor
dot_visitor = DotVisitor.new
dot_visitor.visit(parsed_json)

# Get the DOT format string
dot_format = dot_visitor.to_dot

# Print the DOT Format
puts dot_format