class DotVisitor
  def initialize
    @dot_graph = "digraph ParseTree {\n"
    @node_counter = 0
  end

  def visit(node, parent_id = nil)
    node_id = add_node(node)
    add_edge(parent_id, node_id) if parent_id

    case node
    when Hash
      node.each do |key, value|
        key_id = add_node(key)
        add_edge(node_id, key_id)
        visit(value, node_id)
      end
    when Array
      node.each_with_index do |element, index|
        element_id = add_node(index)
        add_edge(node_id, element_id)
        visit(element, node_id)
      end
    end
  end

  def to_dot
    "#{@dot_graph}}\n"
  end

  private

  def add_node(node)
    @node_counter += 1
    node_id = "node#{@node_counter}"
    label = node_label(node)
    @dot_graph += "#{node_id} [label=\"#{label}\"];\n"
    node_id
  end

  def add_edge(from_id, to_id)
    @dot_graph += "#{from_id} -> #{to_id};\n"
  end

  def node_label(node)
    case node
    when String
      node.inspect
    when Integer, Float, TrueClass, FalseClass, NilClass
      node.to_s
    else
      node.class.name
    end
  end
end
