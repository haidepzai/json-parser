class JsonParser
  def initialize(lexer)
    @lexer = lexer
    @token = @lexer.lex { |l| @lexeme = l }
  end

  def parse
    json_object()
  end

  private

  def check_token(expected)
    if @token == expected
      @token = @lexer.lex { |l| @lexeme = l }
    else
      puts "Syntax error: #{expected} is expected"
      exit(1)
    end
  end

  def json_object
    case @token
    when :lbra
      check_token(:lbra)
      result = {}
      while @token == :string
        key = @lexeme
        check_token(:string)
        check_token(:colon)
        value = json_value
        result[key] = value
        if @token == :comma
          check_token(:comma)
        end
      end
      check_token(:rbra)
    else
      puts "Syntax error: { is expected"
      exit(1)
    end
    result
  end

  def json_value
    case @token
    when :string
      result = @lexeme
      check_token(:string)
    when :int
      result = @lexeme.to_i
      check_token(:int)
    when :float
      result = @lexeme.to_f
      check_token(:float)
    when :true
      result = true
      check_token(:true)
    when :false
      result = false
      check_token(:false)
    when :null
      result = nil
      check_token(:null)
    when :lbra
      result = json_object
    when :lbracket
      result = json_array
    else
      puts "Syntax error: Unexpected token: #{@lexeme}"
      exit(1)
    end
    result
  end

  def json_array
    case @token
    when :lbracket
      check_token(:lbracket)
      result = []
      while @token != :rbracket
        value = json_value
        result << value
        if @token == :comma
          check_token(:comma)
        end
      end
      check_token(:rbracket)
    else
      puts "Syntax error: [ is expected"
      exit(1)
    end
    result
  end
end
