require_relative 'json_lexer'
require_relative 'JsonParser'

# Change to other example as needed
file_path = File.expand_path('../examples/example0.json', __dir__)

lexer = JsonLexer.new(File.open(file_path, 'r'))

parser = JsonParser.new(lexer)

parsed_json = parser.parse

# Print the parsed JSON as a parse tree
puts parsed_json.inspect
