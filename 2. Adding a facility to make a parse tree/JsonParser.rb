class JsonParser
  def initialize(lexer)
    @lexer = lexer
    @token = @lexer.lex { |l| @lexeme = l }
  end

  def parse
    json_object
  end

  private

  def check_token(expected)
    if @token == expected
      @token = @lexer.lex { |l| @lexeme = l }
    else
      puts "Syntax error: #{expected} is expected"
      exit(1)
    end
  end

  def json_object
    case @token
    when :lbra
      check_token(:lbra)
      result = {}
      while @token == :string
        key = @lexeme
        check_token(:string)
        check_token(:colon)
        value = json_value
        result[key] = value
        if @token == :comma
          check_token(:comma)
        end
      end
      check_token(:rbra)
      result
    else
      puts "Syntax error: { is expected"
      exit(1)
    end
  end

  def json_value
    case @token
    when :string
      result = StringNode.new(@lexeme)
      check_token(:string)
    when :int
      result = IntNode.new(@lexeme.to_i)
      check_token(:int)
    when :float
      result = FloatNode.new(@lexeme.to_f)
      check_token(:float)
    when :true
      result = BooleanNode.new(true)
      check_token(:true)
    when :false
      result = BooleanNode.new(false)
      check_token(:false)
    when :null
      result = NullNode.new
      check_token(:null)
    when :lbra
      result = json_object
    when :lbracket
      result = json_array
    else
      puts "Syntax error: Unexpected token: #{@lexeme}"
      exit(1)
    end
    result
  end

  def json_array
    case @token
    when :lbracket
      check_token(:lbracket)
      result = []
      while @token != :rbracket
        value = json_value
        result << value
        if @token == :comma
          check_token(:comma)
        end
      end
      check_token(:rbracket)
      result
    else
      puts "Syntax error: [ is expected"
      exit(1)
    end
  end
end

# Define classes for parse tree nodes
class JsonNode
  attr_reader :value

  def initialize(value)
    @value = value
  end
end

class StringNode < JsonNode; end
class IntNode < JsonNode; end
class FloatNode < JsonNode; end
class BooleanNode < JsonNode; end
class NullNode < JsonNode
  def initialize(value = nil)
    super(value)
  end
end
